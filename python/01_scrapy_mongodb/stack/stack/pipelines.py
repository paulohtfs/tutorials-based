import pymongo

from scrapy.exceptions import DropItem

class MongoDBPipeline(object):
    def process_item(self, item, spider):
        connection = pymongo.MongoClient(
            'localhost',
            username="root",
            password="root"
        )
        db = connection['stackoverflow']

        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:
            db.questions.insert_one(dict(item))
        return item
